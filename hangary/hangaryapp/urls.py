from django.urls import path

from hangaryapp.views import MainView

urlpatterns = [
    path('', MainView.as_view())
]