from django.http import HttpResponse
from django.shortcuts import render
from django.views import View

from hangaryapp import services
from hangaryapp.services import TaskAssignment


class MainView(View):
    template_name = 'hangary.html'

    def get(self, request):
        task_matrix = services.generate_matrix(5)

        context = {'matrix': task_matrix}
        return render(request, self.template_name, context=context)

    def post(self, request):
        post = request.POST
        task_matrix = services.get_matrix_from_post(post, 5)
        return ResultView.get(ResultView(), request, task_matrix)


class ResultView(View):
    template_name = 'result.html'

    def get(self, request, task_matrix):
        # Используйте метод Венгрии для распределения задач
        ass_by_Hun = TaskAssignment(task_matrix, 'Hungary')
        context = {'min_cost': ass_by_Hun.min_cost,
                   'best_solution': ass_by_Hun.best_solution,
                   'task_matrix': task_matrix
                   }
        return render(request, self.template_name, context=context)