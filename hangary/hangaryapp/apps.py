from django.apps import AppConfig


class HangaryappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hangaryapp'
